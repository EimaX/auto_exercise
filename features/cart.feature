Feature: Adding items to cart

  Scenario: Add 2 items to cart and checkout
    When user adds first item from landing page to cart
    And user adds second item from landing page to cart
    And user goes to Cart
    Then user sees two items in the cart
    When user places the order
    Then user can see confirmation of the purchase
