from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common import exceptions
import time


class Browser(object):
    options = Options()
    options.add_argument('--headless')
    options.add_argument('--disable-gpu')

    driver = webdriver.Chrome(ChromeDriverManager().install(), chrome_options=options)
    driver.implicitly_wait(30)
    driver.set_page_load_timeout(30)
    driver.maximize_window()

    def close(self):
        self.driver.close()

    def quit(self):
        self.driver.quit()

    def wait_for_alert(self):
        for i in range(0, 5):
            try:
                alert = self.driver.switch_to.alert
                break
            except exceptions.NoAlertPresentException:
                time.sleep(1)

    def accept_alert(self):
        self.driver.switch_to.alert.accept()