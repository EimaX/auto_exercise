from behave import *
import time

def read_last_3_records():
    with open('data.csv') as f:
        last_rec = list(f)[-3:]
    return last_rec


@when("3 users logins are checked")
def step_impl(context):
    credentials = read_last_3_records()
    for i in range(0, 3):
        user = credentials[i].split(',')[0]
        pwd = credentials[i].split(',')[1]
        context.landing_page.click_login()
        context.landing_page.fill_login(user, pwd)
        context.landing_page.assert_successful_login()
        context.landing_page.logout()
        time.sleep(1)
