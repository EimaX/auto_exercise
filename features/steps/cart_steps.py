from behave import *
import time


@when("user adds first item from landing page to cart")
def step_impl(context):
    context.landing_page.click_n_product(1)
    context.product_page.click_add_to_cart()
    context.browser.wait_for_alert()
    context.browser.accept_alert()
    context.product_page.click_home()


@step("user adds second item from landing page to cart")
def step_impl(context):
    context.landing_page.click_n_product(2)
    context.product_page.click_add_to_cart()
    context.browser.wait_for_alert()
    context.browser.accept_alert()


@step("user goes to Cart")
def step_impl(context):
    context.product_page.click_cart()


@then("user sees two items in the cart")
def step_impl(context):
    cart_size = context.cart_page.get_cart_size()
    assert cart_size == 2


@when("user places the order")
def step_impl(context):
    context.cart_page.click_place_order()
    context.cart_page.fill_form()


@then("user can see confirmation of the purchase")
def step_impl(context):
    context.cart_page.assert_purchase()
