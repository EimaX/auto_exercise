from behave import *
import time
from datetime import datetime


def random_word():
    ts = int(time.time())
    return f'EM{ts}'


def write_to_csv(line):
    with open('data.csv', 'a') as file:
        file.write(line + '\n')


@when("user clicks Sign Up")
def step_impl(context):
    context.landing_page.click_sign_up()


@step("fills the form")
def step_impl(context):
    context.landing_page.fill_sign_up('yy111112', 'uu')


@then("message about successful Sign Up should be visible")
def step_impl(context):
    context.landing_page.assert_successful_signup()


@when("3 users are signed up")
def step_impl(context):
    for i in range(0, 3):
        user = random_word()
        pwd = random_word()
        time.sleep(1)  # todo wait for element
        context.landing_page.click_sign_up()
        context.landing_page.fill_sign_up(user, pwd)
        context.landing_page.assert_successful_signup()
        context.browser.accept_alert()

        c_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        line_to_csv = f'{user},{pwd},{c_time}'
        write_to_csv(line_to_csv)


@then("home page should be displayed")
def step_impl(context):
    context.landing_page.assert_logo_visible()
