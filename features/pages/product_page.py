from features.browser import Browser


class ProductPage(Browser):

    ADD_TO_CART = '//*[@id="tbodyid"]/div[2]/div/a'
    HOME = '(//a[@href="index.html"])[2]'
    CART = '//*[@id="cartur"]'

    def click_add_to_cart(self):
        self.driver.find_element_by_xpath(self.ADD_TO_CART).click()

    def click_home(self):
        self.driver.find_element_by_xpath(self.HOME).click()

    def click_cart(self):
        self.driver.find_element_by_xpath(self.CART).click()