from features.browser import Browser
import time


class CartPage(Browser):
    PLACE_ORDER = '//button[@data-target="#orderModal"]'
    TABLE_ITEMS = '//*[@id="page-wrapper"]/div/div[1]/div/table//tbody/tr'
    FORM_NAME = '//*[@id="name"]'
    FORM_COUNTRY = '//*[@id="country"]'
    FORM_CITY = '//*[@id="city"]'
    FORM_CARD = '//*[@id="card"]'
    FORM_MONTH = '//*[@id="month"]'
    FORM_YEAR = '//*[@id="year"]'
    FORM_PURCHASE_BUTTON = "//button[contains(text(), 'Purchase')]"
    PURCHASE_CONFIRM = "//h2[contains(text(),'Thank you for your purchase')]"

    def click_place_order(self):
        self.driver.find_element_by_xpath(self.PLACE_ORDER).click()

    def get_cart_size(self):
        time.sleep(3)  # todo a smarter wait for this
        elements = self.driver.find_elements_by_xpath(self.TABLE_ITEMS)
        return len(elements)

    def fill_form(self):
        input_string = 'AUTO TESTS'
        self.driver.find_element_by_xpath(self.FORM_NAME).send_keys(input_string)
        self.driver.find_element_by_xpath(self.FORM_COUNTRY).send_keys(input_string)
        self.driver.find_element_by_xpath(self.FORM_CITY).send_keys(input_string)
        self.driver.find_element_by_xpath(self.FORM_CARD).send_keys(input_string)
        self.driver.find_element_by_xpath(self.FORM_MONTH).send_keys(input_string)
        self.driver.find_element_by_xpath(self.FORM_YEAR).send_keys(input_string)
        self.driver.find_element_by_xpath(self.FORM_PURCHASE_BUTTON).click()

    def assert_purchase(self):
        elements = self.driver.find_elements_by_xpath(self.PURCHASE_CONFIRM)
        assert len(elements) > 0
