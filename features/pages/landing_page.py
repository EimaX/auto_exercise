from features.browser import Browser
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class LandingPage(Browser):
    url = "https://www.demoblaze.com/"
    successful_signup_message = 'Sign up successful.'

    LOGIN = '//*[@id="login2"]'
    SIGNUP = '//*[@id="signin2"]'
    FIRST_PRODUCT = '//*[@id="tbodyid"]/div[1]/div/a'
    HOME = '(//a[@href="index.html"])[2]'
    LOGO = '(//a[@href="index.html"])[1]'
    SIGNUP_USERNAME = '//*[@id="sign-username"]'
    SIGNUP_PASSWORD = '//*[@id="sign-password"]'
    LOGIN_USERNAME = '//*[@id="loginusername"]'
    LOGIN_PASSWORD = '//*[@id="loginpassword"]'
    BTN_SIGNUP = '//button[contains(text(), "Sign up")]'
    BTN_LOGIN = '//button[contains(text(), "Log in")]'
    WELCOME_TEXT = '//a[contains(text(), "Welcome")]'
    LOGOUT = '//*[@id="logout2"]'

    def get_page(self):
        self.driver.get(self.url)

    def click_n_product(self, n=1):
        self.driver.find_element_by_xpath(f'//*[@id="tbodyid"]/div[{n}]/div/a').click()

    def click_sign_up(self):
        WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.XPATH, self.SIGNUP)))
        self.driver.find_element_by_xpath(self.SIGNUP).click()

    def click_login(self):
        self.driver.find_element_by_xpath(self.LOGIN).click()

    def fill_login(self, username, password):
        self.driver.find_element_by_xpath(self.LOGIN_USERNAME).clear()
        self.driver.find_element_by_xpath(self.LOGIN_USERNAME).send_keys(username)
        self.driver.find_element_by_xpath(self.LOGIN_PASSWORD).clear()
        self.driver.find_element_by_xpath(self.LOGIN_PASSWORD).send_keys(password)
        self.driver.find_element_by_xpath(self.BTN_LOGIN).click()

    def fill_sign_up(self, username, password):
        self.driver.find_element_by_xpath(self.SIGNUP_USERNAME).clear()
        self.driver.find_element_by_xpath(self.SIGNUP_USERNAME).send_keys(username)
        self.driver.find_element_by_xpath(self.SIGNUP_PASSWORD).clear()
        self.driver.find_element_by_xpath(self.SIGNUP_PASSWORD).send_keys(password)
        self.driver.find_element_by_xpath(self.BTN_SIGNUP).click()

    def assert_successful_signup(self):
        self.wait_for_alert()
        alert_text = self.driver.switch_to.alert.text
        assert self.successful_signup_message in alert_text, 'Got: ' + alert_text

    def accept_alert(self):
        self.driver.switch_to.alert.accept()

    def assert_logo_visible(self):
        element_count = self.driver.find_elements_by_xpath(self.LOGO)
        assert len(element_count) > 0

    def assert_successful_login(self):
        element_count = self.driver.find_elements_by_xpath(self.WELCOME_TEXT)
        assert len(element_count) > 0

    def logout(self):
        self.driver.find_element_by_xpath(self.LOGOUT).click()
