from features.browser import Browser
from features.pages.landing_page import LandingPage
from features.pages.product_page import ProductPage
from features.pages.cart_page import CartPage


def before_all(context):
    context.browser = Browser()
    context.landing_page = LandingPage()
    context.product_page = ProductPage()
    context.cart_page = CartPage()

    context.landing_page.get_page()


def after_all(context):
    context.browser.quit()
