<h1>Installation </h1>
<code> pip install -r requirments.txt </code>
<br>
<br>
<b>Packages that will be installed:</b>
<br><br>
behave - BDD implementation <br>
selenium - browser automation <br>
webdriver-manager  - for automatic selenium driver installation 

<h1>Run </h1>
<br> 
To execute the tests simply run <br> <br>
<code>behave</code>
<br>
<br>
<br>
<b>Contact: </b>
<br>
https://www.linkedin.com/in/eima